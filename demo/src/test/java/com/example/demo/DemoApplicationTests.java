package com.example.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class DemoApplicationTests {

	@Autowired
	private ComponentOne componentOne;

	@Autowired
	private ComponentTwo componentTwo;

	@Autowired
	private MockMvc mockMvc;

	@Test
	void contextLoads() {
		String value = componentOne.getStudentNameForId("hola");
		assertEquals("student_TEST_named_hola", value);

		when(componentTwo.getStudentNameForId(anyString())).thenReturn("hola");

		value = componentTwo.getStudentNameForId("hola");
		assertEquals("hola", value);
	}

	@Test
	void controllerTest() throws Exception {
		MvcResult mvcResult = this.mockMvc
				.perform(get("/student/abc"))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();
				//.andExpect(content().string(containsString("Hello World")));

		final String json = mvcResult.getResponse().getContentAsString();

		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> result = mapper.readValue(json, Map.class);

		assertEquals("student_TEST_named_abc", result.get("studentId"));
	}
}
