package com.example.demo;

import org.springframework.stereotype.Component;

@Component
public interface ComponentTwo {
    String getStudentNameForId(String id);
}
