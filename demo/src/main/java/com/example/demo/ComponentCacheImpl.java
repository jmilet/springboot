package com.example.demo;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ComponentCacheImpl implements ComponentCache {

    private static HashMap<String, Object> cache = new HashMap();

    @Override
    public synchronized void put(final String key, final Object value) {
        cache.put(key, value);
    }

    @Override
    public synchronized Object get(final String key) {
        return cache.get(key);
    }

    @Override
    public synchronized Set<Map.Entry<String, Object>> getKeyValues() {
        return cache.entrySet();
    }
}
