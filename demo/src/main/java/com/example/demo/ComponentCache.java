package com.example.demo;

import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Set;

@Component
public interface ComponentCache {
    void put(String key, Object value);
    Object get(String key);
    Set<Map.Entry<String, Object>> getKeyValues();
}
