package com.example.demo;

public class ComponentOneImpl2 implements ComponentOne {
    @Override
    public String getStudentNameForId(String id) {
        return String.format("student_TEST_named_%s", id);
    }
}
