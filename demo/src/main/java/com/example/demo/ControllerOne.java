package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@RestController
public class ControllerOne {

    @Autowired
    private ComponentOne componentOne;

    @Autowired
    private ComponentCache componentCache;

    @Autowired
    private RedisRepository redisRepository;

    @GetMapping(value = "/student/{studentId}")
    public ResponseEntity<Map<String, Object>> getData(@PathVariable String studentId) {
        Map<String, Object> value = (Map<String, Object>) componentCache.get(studentId);
        Map<String, Object> response = null;
        HttpStatus httpStatus = HttpStatus.OK;

        if (value != null) {
            response = new HashMap<>(value);
            response.put("cache", true);
        }
        else {
            if (studentId.contains("a")) {
                value = new HashMap<String, Object>() {{
                    put("studentId", componentOne.getStudentNameForId(studentId));
                }};

                componentCache.put(studentId, value);

                response = new HashMap<>(value);
                response.put("cache", false);
            }
            else {
                httpStatus = HttpStatus.NOT_FOUND;
            }
        }

        return new ResponseEntity(response, httpStatus);
    }

    @GetMapping(value = "/cache")
    public Set<Map.Entry<String, Object>> getKeyValues() {
        return componentCache.getKeyValues();
    }

    @GetMapping(value = "/redis/set/{id}/{value}")
    public ResponseEntity<Object> setRedisRecord(
            @PathVariable final String id,
            @PathVariable final String value)
    {
        RedisRecord record = new RedisRecord();

        record.setId(id);
        record.setValue(value);

        redisRepository.save(record);

        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @GetMapping(value = "/redis/get/{id}")
    public ResponseEntity<Object> getRedisRecord(
            @PathVariable final String id)
    {
        final Optional<RedisRecord> record = redisRepository.findById(id);

        if (!record.isPresent()) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(record, HttpStatus.OK);
    }
}
