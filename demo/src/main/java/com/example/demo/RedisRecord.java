package com.example.demo;

import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;

@RedisHash("RedisRecord")
public class RedisRecord implements Serializable {
    private String id;
    private String value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
