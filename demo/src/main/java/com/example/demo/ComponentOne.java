package com.example.demo;

import org.springframework.stereotype.Component;

@Component
public interface ComponentOne {
    String getStudentNameForId(String id);
}
