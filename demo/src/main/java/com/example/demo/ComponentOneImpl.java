package com.example.demo;

public class ComponentOneImpl implements ComponentOne {
    @Override
    public String getStudentNameForId(String id) {
        return String.format("student_named_%s", id);
    }
}
